#!/bin/bash

function test1_ {

COUNTER_SUCCESS=0
COUNTER_FAIL=0
DIFF_RES=""
echo "" > log.txt

for var in -v -c -l -n -h -o
do
  for var2 in -v -c -l -n -h -o
  do
        if [ $var != $var2 ]
        then
          TEST1="for grep.c grep.h Makefile $var $var2"
          echo "$TEST1"
          ./grep $TEST1 > my_grep.txt
          grep $TEST1 > grep.txt
          DIFF_RES="$(diff -s my_grep.txt grep.txt)"
          # if [ "$DIFF_RES" == "Files my_grep.txt and grep.txt are identical" ]
          if [ "$DIFF_RES" == "Файлы my_grep.txt и grep.txt идентичны" ]
            then
              (( COUNTER_SUCCESS++ ))
            else
              echo "$TEST1" >> log.txt
              (( COUNTER_FAIL++ ))
          fi
          rm my_grep.txt grep.txt

          TEST2="for grep.c $var $var2"
          echo "$TEST2"
          ./grep $TEST2 > my_grep.txt
          grep $TEST2 > grep.txt
          DIFF_RES="$(diff -s my_grep.txt grep.txt)"
          # if [ "$DIFF_RES" == "Files my_grep.txt and grep.txt are identical" ]
          if [ "$DIFF_RES" == "Файлы my_grep.txt и grep.txt идентичны" ]
            then
              (( COUNTER_SUCCESS++ ))
            else
              echo "$TEST2" >> log.txt
              (( COUNTER_FAIL++ ))
          fi
          rm my_grep.txt grep.txt
        fi
  done
done

echo "SUCCESS: $COUNTER_SUCCESS"
echo "FAIL: $COUNTER_FAIL"

echo "SUCCESS: $COUNTER_SUCCESS" >> result.txt
echo "FAIL: $COUNTER_FAIL" >> result.txt

if (( COUNTER_FAIL > 0 )); then
  COUNTER_FAIL=1
fi

return $COUNTER_FAIL

}
